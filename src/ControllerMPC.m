classdef ControllerMPC < ControllerBase
    properties
        Q_;
        R_;
        N_;
        Qf_;

        state_upper_bound_;
        state_lower_bound_;

        qp_static_formulated_;
        static_A_qp_;
        static_b_qp_;
        qp_ops_ = optimset('Display','off');
    end

    methods
        function self = ControllerMPC(ArmModel)
            self@ControllerBase(ArmModel);
            self.name_ = 'MPC';
            % default value
            self.set_Q_R(eye(4), eye(2));
            self.set_N(20);
            self.state_upper_bound_ = 100 * ones(4, 1);
            self.state_lower_bound_ = -100 * ones(4, 1);
            % turn off warning for qp solver
            warning('off', 'all');
        end

        function u = get_input(self, x)
            nu = 2;
            optarg = self.solve_qp(x);
            u = optarg(1:nu, 1);
        end

        function optarg = solve_qp(self, x)
            % some part of qp only needs to be formulated once
            if ~self.qp_static_formulated_
                self.formulate_qp_static();
            end

            [H_qp, f_qp, A_qp, b_qp, Aeq_qp, beq_qp] = ...
                self.formulate_qp_dynamic(x, zeros(2, 1));
            try
                A_qp = [A_qp; self.static_A_qp_];
                b_qp = [b_qp; self.static_b_qp_];
            catch
                A_qp = self.static_A_qp_;
                b_qp = self.static_B_qp_;
            end

            % optarg = [u1; ...; uN; x0]
            optarg = quadprog(...
                H_qp, f_qp, A_qp, b_qp, Aeq_qp, beq_qp, [], [], [], self.qp_ops_);
        end

        function set_N(self, N)
            self.N_ = N;
            self.qp_static_formulated_ = false;
        end

        function set_Q_R(self, Q, R)
            self.Q_ = Q;
            self.R_ = R;

            % TODO: double check the way to compute Qf (probably wrong here,
            % but shouldn't have significant consequence)
            [Ad, Bd, ~] = self.ArmModel_.get_discrete_system(...
                zeros(4, 1), zeros(2, 1), self.dt_);
            self.Qf_ = dare(Ad, Bd, Q, R);

            self.qp_static_formulated_ = false;
        end

        function formulate_qp_static(self)
            % Brief: return x and u independent QP model of MPC. This function
            % should only be called once.

            [self.static_A_qp_, self.static_b_qp_] = self.formulate_input_constraint();
            self.qp_static_formulated_ = true;
        end

        function [H_qp, f_qp, A_qp, b_qp, Aeq_qp, beq_qp] = formulate_qp_dynamic(self, x, u)
            % state transition
            [S_x, S_u, S_c] = self.formulate_state_transition(x, u);
            % cost
            [H_qp, f_qp] = self.formulate_cost(S_x, S_u, S_c);
            % state constraint
            [A_qp, b_qp] = self.formulate_state_constraint(S_x, S_u, S_c);
            % initial state constraints
            [Aeq_qp, beq_qp] = self.formulate_initial_state_constraint(x);
        end

        function [S_x, S_u, S_c] = formulate_state_transition(self, x, u)
            nx = 4;
            nu = 2;

            % compute discrete system dynamics x_k+1 = Ad * x_k + Bd * u_k + Cd
            xbar = x;
            ubar = zeros(nu, 1);
            % u0_ = zeros(nu, 1);
            [Ad, Bd, Cd] = self.ArmModel_.get_discrete_system(xbar, ubar, self.dt_);

            % convert MPC objective (cost) to QP objective
            % [x1, x2, ... , xN] =
            % S_x * x0 + S_u * [u1, u2, ..., un] * S_c
            S_x = zeros(self.N_ * nx, nx);
            S_u = zeros(self.N_ * nx, self.N_ * nu);
            S_c = zeros(self.N_ * nx, self.N_ * nx);
            for i = 1:self.N_
                S_x((i-1)*nx+1 : i*nx, :) = Ad^i;
                for j = 1:i
                    S_u((i-1)*nx+1 : i*nx, (j-1)*nu+1 : j*nu) = Ad^(i-j) * Bd;
                    S_c((i-1)*nx+1 : i*nx, (j-1)*nx+1 : j*nx) = Ad^(i-j);
                end
            end
            S_c = S_c * repmat(Cd, self.N_, 1);
        end

        function [H_qp, f_qp] = formulate_cost(self, S_x, S_u, S_c)
            nx = 4;

            Qbar = self.get_blkdiag(self.Q_, self.N_);
            Qbar((self.N_-1)*nx+1:end, (self.N_-1)*nx+1:end) = self.Qf_;
            Rbar = self.get_blkdiag(self.R_, self.N_);
            H = S_u' * Qbar * S_u + Rbar;
            F = S_x' * Qbar * S_u;
            Y = S_x' * Qbar * S_x + self.Q_;
            H_qp = 2 * [H, F'; F, Y];
            x_goal_rep = repmat(self.x_goal_, self.N_, 1);
            temp = 2 * Qbar * (S_c - x_goal_rep);
            f_qp = [S_u' * temp; S_x' * temp - 2 * self.Q_ * self.x_goal_];
        end

        function [A_qp, b_qp] = formulate_state_constraint(self, S_x, S_u, S_c)
            nx = 4;

            temp = [eye(nx * self.N_); - eye(nx * self.N_)];
            A_qp = temp * [S_u, S_x];
            b_qp = [repmat(self.state_upper_bound_, self.N_, 1); ...
                    -repmat(self.state_lower_bound_, self.N_, 1)] ...
                    + [-S_c; S_c];






            % state constraints |x4| < 1.5
            % temp = [self.get_blkdiag([0, 0, 0, 1], self.N_); ...
            %     self.get_blkdiag([0, 0, 0, -1], self.N_)];
            % A_qp = temp * [S_u, S_x];
            % b_qp = 1.5 * ones(2 * self.N_, 1) - temp * S_c;
            % if tube_mpc
            %     b_qp = b_qp - [0,0,0,1]*(max_state_error + Bd*max_u_tube) * ones(2*N, 1);

            %     % A_qp_ = [zeros(2, N*nu+nx); A_qp_];
            %     % A_qp_(1:2, end) = [1; -1];
            %     % b_qp_ = [2.5 * ones(2, 1); b_qp_];
            %     % b_qp_ = b_qp_ - max_state_error(4) * ones(2*(N+1), 1);
            % end
        end

        function [Aeq_qp, beq_qp] = formulate_initial_state_constraint(self, x)
            nx = 4;
            nu = 2;

            A_x = eye(nx);
            Aeq_qp = [zeros(nx, self.N_ * nu), A_x];
            beq_qp = x;

            % if tube_mpc
            %     A_x_ = [eye(nx); -eye(nx)];
            %     x_max_ = x0_ + max_state_error;
            %     x_min_ = x0_ - max_state_error;
            %     b_x_ = [x_max_; -x_min_];
            %     A_qp_ = [A_qp_; zeros(2*nx, N*nu), A_x_];
            %     b_qp_ = [b_qp_; b_x_];

            %     % no equality constrain
            %     Aeq_qp_ = [];
            %     beq_qp_ = [];
            % else
            %     A_x_ = eye(nx);
            %     Aeq_qp_ = [zeros(nx, N*nu), A_x_];
            %     beq_qp_ = x0_;
            % end
        end

        function [A_qp, b_qp] = formulate_input_constraint(self)
            % TODO: get ride of nx, nu
            nu = 2;
            nx = 4;

            % input constraints
            umax = self.ArmModel_.actuator_max_torque_;
            b_u = [umax; umax];
            % if tube_mpc
            %     b_u_ = b_u_ - max_u_tube;
            % end
            b_u = repmat(b_u, nu, 1);
            A_u = [eye(nu); -eye(nu)];
            G_u = self.get_blkdiag(A_u, self.N_);
            A_qp = [G_u, zeros(2 * self.N_ * nu, nx)];
            b_qp = repmat(b_u, self.N_, 1);
        end

    end

    methods (Static)

        function out = get_blkdiag(in, n)
            temp = repmat(in, 1, n);
            temp = mat2cell(temp, size(in, 1), repmat(size(in, 2), 1, n));
            out = blkdiag(temp{:});
        end

    end
end