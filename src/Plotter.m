classdef Plotter
    methods (Static)

        function plot_result(results)
            colors = {'r', 'g', 'b'};
            names = cell(1, length(results));
            for i = 1:length(results)
                names{i} = results{i}.name_;
            end

            figure();

            % state plot
            subplot_ylabels = {'q1 [rad]', 'q2 [rad]', 'dq1 [rad/s]', 'dq2 [rad/s]'};
            subplot_locations = [1, 2, 4, 5];
            for i = 1:4
                subplot(2, 3, subplot_locations(i));
                hold on;
                p = [];    % save plot handle for adding legend
                for j = 1:length(results)
                    p = [p, plot(results{j}.t_, results{j}.x_(i, :), colors{j})];
                    % plot state bound (for tube-mpc only)
                    if length(results{j}.t_sample_) >= 1
                        plot(results{j}.t_sample_, ...
                            results{j}.x_tube_(i, :) + results{j}.x_tube_bound_(i), ...
                            strcat(colors{j}, '.'), ...
                            results{j}.t_sample_, ...
                            results{j}.x_tube_(i, :) - results{j}.x_tube_bound_(i), ...
                            strcat(colors{j}, '.'));
                    end
                end
                xlabel('time [sec]');
                ylabel(subplot_ylabels{i});
                grid on;
                if i == 2, legend(p, names); end
            end

            % input plot
            subplot_ylabels = {'\tau_{1} [N.m]', '\tau_{2} [N.m]'};
            subplot_locations = [3, 6];
            for i = 1:2
                subplot(2, 3, subplot_locations(i));
                hold on;
                for j = 1:length(results)
                    plot(results{j}.t_, results{j}.u_desire_(i, :), strcat(colors{j}, '--'), ...
                        results{j}.t_, results{j}.u_actual_(i, :), colors{j});
                end
                xlabel('time [sec]');
                ylabel(subplot_ylabels{i});
                grid on;
                if i == 1, legend('desire', 'actual'); end
            end

        end

        function play_result(results)
            % TODO
        end
    end
end