classdef ControllerTubeMPC < ControllerMPC
    properties
        state_tube_bound_;
        TubeController_;
    end

    methods
        function self = ControllerTubeMPC(ArmModel)
            self@ControllerMPC(ArmModel);
            self.name_ = strcat('Tube-', self.name_);
            % default value
            self.TubeController_ = ControllerBase(ArmModel);
            self.state_tube_bound_ = [0.5; 0.5; 0.3; 0.3];
        end

        function set_tube_controller(self, Controller)
            self.TubeController_ = Controller;
            self.qp_static_formulated_ = false;
        end

        function [u, x_tube] = get_input(self, x)
            nu = 2;
            nx = 4;
            optarg = self.solve_qp(x);
            x_tube = optarg(end-nx+1:end, 1);
            self.TubeController_.set_x_goal(x_tube);
            u = optarg(1:nu, 1) + self.TubeController_.get_input(x);
        end

        function [H_qp, f_qp, A_qp, b_qp, Aeq_qp, beq_qp] = formulate_qp_dynamic(self, x, u)
            [S_x, S_u, S_c] = self.formulate_state_transition(x, u);
            [H_qp, f_qp] = self.formulate_cost(S_x, S_u, S_c);
            [A_qp, b_qp] = self.formulate_state_constraint(S_x, S_u, S_c);
            [A_qp2, b_qp2] = self.formulate_initial_state_constraint(x);
            A_qp = [A_qp; A_qp2];
            b_qp = [b_qp; b_qp2];
            Aeq_qp = [];
            beq_qp = [];
        end

        function [A_qp, b_qp] = formulate_input_constraint(self)
            nu = 2;

            [A_qp, b_qp] = formulate_input_constraint@ControllerMPC(self);
            % tighten input constraint by maximum possible tube feedback
            max_tube_input = abs(self.TubeController_.get_input(self.state_tube_bound_));
            temp = repmat(max_tube_input, nu, 1);
            temp = repmat(temp, self.N_, 1);
            b_qp = b_qp - temp;
        end

        function [A_qp, b_qp] = formulate_initial_state_constraint(self, x)
            nx = 4;
            nu = 2;

            A_qp = [zeros(2 * nx, self.N_ * nu), [eye(nx); -eye(nx)]];
            b_qp = [x; -x] + repmat(self.state_tube_bound_, 2, 1);
        end

        function [A_qp, b_qp] = formulate_state_constraint(self, S_x, S_u, S_c)
            nx = 4;
            nu = 2;

            [A_qp, b_qp] = formulate_state_constraint@ControllerMPC(self, S_x, S_u, S_c);
            % tighten state constraint for robust constraint satisfaction
            max_bound = self.state_tube_bound_ + ...
                abs(S_u(1:nx, 1:nu) ...
                    * self.TubeController_.get_input(self.state_tube_bound_));
            b_qp = b_qp - repmat(max_bound, 2 * self.N_, 1);
        end
    end
end