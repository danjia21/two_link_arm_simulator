close all
clear
clc

% TODO: get ride of this
addpath([pwd '/arm_model_functions']);
addpath([pwd '/fixed_step_odes'])

M = ArmModel();

% setting
x_goal = ([pi/2; 0; 0; 0]);
Q = diag([10, 10, 1, 1]);
R = diag([1, 1]) * 10;
dt = 0.1;

ConRL = ControllerRL(M);
