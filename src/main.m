close all
clear
clc

% TODO: get ride of this
addpath([pwd '/arm_model_functions']);
addpath([pwd '/fixed_step_odes'])

M = ArmModel();

% setting
x_goal = ([pi/2; 0; 0; 0]);
Q = diag([10, 10, 1, 1]);
R = diag([1, 1]) * 10;
dt = 0.1;

% LQR
ConLQR = ControllerLQR(M);

% MPC
ConMPC = ControllerMPC(M);
ConMPC.set_N(20);

% Tube-MPC
ConTMPC = ControllerTubeMPC(M);
ConTMPC.set_N(20);
ConT = ControllerLQR(M);
ConT.set_Q_R(Q, 20 * R);
ConTMPC.set_tube_controller(ConT);

% use same parameters on all three controllers
Cons = {ConLQR, ConMPC, ConTMPC};
for i = 1:length(Cons)
    Cons{i}.set_Q_R(Q, R);
    if i == 1, Cons{i}.set_Q_R(Q, 20 * R); end
    Cons{i}.set_x_goal(x_goal);
    Cons{i}.set_dt(dt);
end

% run simulation for each controller
Sims = cell(1, length(Cons));
rand_seq = randn(1, 1e6);    % pre-generate random numbers for disturbance
for i = 1:length(Sims)
    Sims{i} = Simulation();
    Sims{i}.run(M, Cons{i}, rand_seq);
end

% plot results
Plotter.plot_result(Sims);
