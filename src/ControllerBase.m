classdef ControllerBase < handle
    properties
        g_ = 9.81;
        x_goal_;
        dt_;
        ArmModel_;
        name_;
    end

    methods
        function self = ControllerBase(ArmModel)
            self.ArmModel_ = ArmModel;
            % default value
            self.x_goal_ = zeros(4, 1);
            self.dt_ = 1e-1;
        end

        function u = get_input(self, x)
            u = zeros(2, 4) * (x - self.x_goal_);
        end

        function set_x_goal(self, x_goal)
            self.x_goal_ = x_goal;
        end

        function set_dt(self, dt)
            self.dt_ = dt;
        end
    end
end