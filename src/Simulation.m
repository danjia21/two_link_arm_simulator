classdef Simulation < handle
    properties

        name_;

        % time
        dt_ = 5e-4;
        t0_ = 0;
        t1_ = 10;

        % noise characteristics
        x_noise_std_ = 5e-5;  % measurement noise
        u_noise_std_ = 1e-3;  % disturbance
        udot_noise_std_ = 1;  % disturbance on udot (actuator dynamics)

        % initial and final condition
        x0_ = [pi/2; pi/2; 0; 0];
        u0_ = zeros(2, 1);

        % result
        t_;
        t_sample_;
        x_;
        u_desire_;
        u_actual_;
        x_tube_;
        x_tube_bound_;
    end

    methods

        function run(self, ArmModel, Controller, rand_seq)
            disp(Controller.name_);

            % init
            self.t_ = self.t0_ : self.dt_ : self.t1_;
            nt = length(self.t_);
            self.x_ = zeros(4, nt);
            self.u_desire_ = zeros(2, nt);
            self.u_actual_ = zeros(2, nt);
            self.t_sample_ = zeros(1, nt);
            self.x_tube_ = zeros(4, nt);
            last_sample_time = -1;
            last_t_display_time = -100;
            display_t_intervel = 1.0;
            idx_sample = 1;
            idx_rand = 1;

            % initial state
            self.x_(:, 1) = self.x0_;
            self.u_actual_(:, 1) = self.u0_;

            % main loop
            for i=1:nt-1

                % display time
                if self.t_(i) - last_t_display_time >= display_t_intervel
                    disp(self.t_(i))
                    last_t_display_time = self.t_(i);
                end

                % measured state
                x0_real = self.x_(:, i);
                x0_measure = x0_real + ...
                    self.x_noise_std_ * rand_seq(idx_rand:idx_rand+4-1)';
                idx_rand = idx_rand + 4;

                % desired input
                if self.t_(i) - last_sample_time >= Controller.dt_
                    try
                        % for tube-mpc
                        [u0_desire, self.x_tube_(:, idx_sample)] = ...
                            Controller.get_input(x0_measure);
                        self.t_sample_(idx_sample) = self.t_(i);
                        idx_sample = idx_sample + 1;
                    catch
                        u0_desire = Controller.get_input(x0_measure);
                    end
                    last_sample_time = self.t_(i);
                else
                    u0_desire = self.u_desire_(:, i-1);
                    self.x_tube_(:, i) = self.x_tube_(:, i-1);
                end

                % actual input
                u0_real = self.u_actual_(:, i) + ...
                    self.u_noise_std_ * rand_seq(idx_rand:idx_rand+2-1)';
                idx_rand = idx_rand + 2;

                % system dynamics (link and actuator)
                xdot = ArmModel.get_xdot_full([x0_real; u0_real], u0_desire);
                udot_real = xdot(5:6) + ...
                    self.udot_noise_std_ * rand_seq(idx_rand:idx_rand+2-1)';
                idx_rand = idx_rand + 2;

                % store result
                self.u_desire_(:, i) = u0_desire;
                self.u_actual_(:, i+1) = u0_real + self.dt_ * udot_real;
                self.x_(:, i+1) = x0_real + self.dt_ * xdot(1:4);

                % if reached the end of random number sequence, start from
                % beginning again
                if idx_rand > length(rand_seq) - 20
                    idx_rand = 1;
                end
            end

            % shaping data
            if idx_sample == 1
                self.t_sample_ = [];
                self.x_tube_ = [];
                self.x_tube_bound_ = [];
            else
                self.t_sample_ = self.t_sample_(:, 1:idx_sample-1);
                self.x_tube_ = self.x_tube_(:, 1:idx_sample-1);
                self.x_tube_bound_ = Controller.state_tube_bound_;
            end
            self.u_desire_(:, end) = self.u_desire_(:, end-1);
            self.name_ = Controller.name_;
        end

        function run_ode45(self, ArmModel, Controller)
            % TODO
        end

        function [t, x, u_desire, u_actual] = get_result(self)
            t = self.t_;
            x = self.x_;
            u_desire = self.u_desire_;
            u_actual = self.u_actual_;
        end

    end

end