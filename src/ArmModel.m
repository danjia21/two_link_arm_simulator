classdef ArmModel < handle
    properties
        actuator_max_torque_ = 5;
        actuator_low_pass_tau_ = 0.5;
    end

    methods (Static)

        function [A, B] = get_linearized_system(xbar, ubar)
            q = xbar(1:2);
            qdot = xbar(3:4);
            A = [Arm_f1_q(q, qdot, ubar), Arm_f1_dq(q, qdot, ubar); ...
                Arm_f2_q(q, qdot, ubar), Arm_f2_dq(q, qdot, ubar)];
            B = [Arm_f1_tau(q, qdot, ubar); ...
                Arm_f2_tau(q, qdot, ubar)];
        end

    end

    methods

        function xdot = get_xdot_full(self, x, u_request)
            q = x(1:4);
            u = x(5:6);

            xdot_arm = self.get_xdot_arm(q, u);
            udot_actuator = self.get_udot_actuator(u, u_request);

            xdot = [xdot_arm; udot_actuator];
        end

        function xdot = get_xdot_arm(self, x, u)
            q = x(1:2);
            qdot = x(3:4);

            % arm dynamics
            M = Arm_M(q);
            B = Arm_B(q, qdot);
            G = Arm_G(q);
            S = Arm_S(q);
            qddot = M \ (S' * u - B - G);

            xdot = [qdot; qddot];
        end

        function udot = get_udot_actuator(self, u, u_request)
            % actuator dynamics
            u_saturate = self.saturate_input(u_request);
            udot = (u_saturate - u) / self.actuator_low_pass_tau_;
        end



        function [Ad, Bd, Cd] = get_discrete_system(self, xbar, ubar, dt)
            nx = length(xbar);
            nu = length(ubar);
            [A, B] = self.get_linearized_system(xbar, ubar);
            temp = expm(dt * [A, B; zeros(nu, nu+nx)]);
            Ad = temp(1:nx, 1:nx);
            Bd = temp(1:nx, nx+1:end);
            Cd = dt * (self.get_xdot_arm(xbar, ubar) - A * xbar - B * ubar);
        end

        function u_out = saturate_input(self, u_in)
            u_out = min(self.actuator_max_torque_, ...
                max(-self.actuator_max_torque_, u_in));
        end

    end

end