classdef ControllerLQR < ControllerBase
    properties
        Q_;
        R_;
        K_;
    end

    methods
        function self = ControllerLQR(ArmModel)
            self@ControllerBase(ArmModel);
            self.name_ = 'LQR';
            % default value
            self.set_Q_R(eye(4), eye(2));
        end

        function u = get_input(self, x)
            u = - self.K_ * (x - self.x_goal_);
        end

        function set_Q_R(self, Q, R)
            self.Q_ = Q;
            self.R_ = R;

            [A, B] = self.ArmModel_.get_linearized_system(self.x_goal_, zeros(2, 1));
            self.K_ = lqrd(A, B, Q, R, self.dt_);
        end
    end
end