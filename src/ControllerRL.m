classdef ControllerRL < ControllerBase
    properties
        Q_ = eye(4);
        R_ = eye(2);
        c_;
        gma_ = 0.7;
        ep_ = 0.2;
        n_ = 4;
        rand_ = rand(1e8, 1);
        rand_idx_ = 0;
        action_space_bin_ = 5;
        action_space_;
        w_;
    end

    methods
        function self = ControllerRL(ArmModel)
            self@ControllerBase(ArmModel);
            self.name_ = 'RL';

            % feature and weight
            tmp = [0.5 * eye(4); eye(4); 1.5 * eye(4)];
            tmp = padarray(tmp, [0, 2], 'post');
            tmp2 = [0.5 * eye(2); eye(2); 1.5 * eye(2)];
            tmp2 = padarray(tmp2, [0, 4], 'pre');
            self.c_ = [tmp; tmp2] * pi;
            self.w_ = ones(1, size(self.c_, 1));

            % action space
            umax = ArmModel.actuator_max_torque_;
            u = linspace(-umax, umax, self.action_space_bin_);
            u1 = meshgrid(u);
            u1 = reshape(u1, [1, self.action_space_bin_^2]);
            u2 = repmat(u, 1, self.action_space_bin_);
            self.action_space_ = [u1; u2];
        end

        function u = get_input(self, x)
            min_val = 1e20;
            u = zeros(2, 1);

            % randomly select
            if self.get_rand() < self.ep_
                idx = randi(self.action_space_bin_^2);
                u = self.action_space_(:, idx);
                return;
            end

            % select best
            for i = 1 : size(self.action_space_, 2)
                val = self.w_ * self.get_feature(x, self.action_space_(:, i));
                if val < min_val
                    min_val = val;
                    u = self.action_space_(:, i);
                end
            end
        end

        function s = get_feature(self, x, u)
            s = cos(self.c_ * [x - self.x_goal_; u]);
        end

        function r = get_stage_cost(self, x, u)
            r = (x-x_goal_) * self.Q_ * (x-self.x_goal_)' + u * self.R_ * u';
        end

        function r = get_rand(self)
            self.rand_idx_ = self.rand_idx_ + 1;
            r = self.rand_(self.rand_idx_);
        end

    end
end