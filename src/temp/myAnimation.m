function myAnimation(body,q,dt)

num_joint = length(body);
r = 0;
for cnt = 1:num_joint
    r = r + double(body(cnt).param.l);
end

% 3D position of joints + end-effector
end_points = sym(zeros(3,num_joint+1));
for cnt = 1:num_joint
    end_points(:,cnt) = sym(body(cnt).kin.I_r_O);
end
l_end = sym(['l' num2str(num_joint)]);
temp = body(end).kin.A_IB*sym([body(end).param.l;0;0]) + body(end).kin.I_r_O;
end_points(:,end) = sym(temp);

q_ = sym(zeros(num_joint,1));
for cnt = 1:num_joint
    q_(cnt)  = sym(sprintf('q%d',cnt),'real');
end
point_fcn = matlabFunction(end_points(1:2,:),'vars',{q_});

points = zeros(num_joint,3,size(q,2));
for cnt = 1:size(q,2)
    points(:,:,cnt) = point_fcn(q(:,cnt));
end


c = -pi:.04:pi;
cx = points(1,1,1)+r*cos(c);
cy = points(2,1,1)-r*sin(c);
hf = figure('color','white');

axis off, axis equal;
hold on
line(cx, cy, 'color', [0.7 0.7 0.7],'LineWidth',2);
title(sprintf('%d-Link Arm',num_joint),'Color',[.6 0 0])
x = points(1,:,1);
y = points(2,:,1);

hf = plot(x, y, 'color', [.4 .4 .8],'LineWidth',3);
set(hf,'XDataSource','x')
set(hf,'YDataSource','y')

for cnt = 1
    x = points(1,:,cnt);
    y = points(2,:,cnt);
    refreshdata(hf,'caller');
    pause(dt);
end

for cnt = 2:10:size(points,3)
    x = points(1,:,cnt);
    y = points(2,:,cnt);
    refreshdata(hf,'caller');
    pause(dt);
end





