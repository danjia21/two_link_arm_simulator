function rand_var = noise_sequence()
    % Brief: return a struct with pre-generated random noise

    mem = 1000000;
    idx = 1;
    noise_seq = randn(mem, 1);

    rand_var = struct;
    rand_var.get_input_noise = @get_input_noise;
    rand_var.get_state_noise = @get_state_noise;
    rand_var.reinit = @reinit;

    function v_input_ = get_input_noise()
        v_input_ = 0.1 * noise_seq(idx:idx+1, 1);
        idx = idx + 2;
        if idx > length(noise_seq) - 10
            reinit();
        end
    end

    function v_state_ = get_state_noise()
        v_state_ = 0.05 * noise_seq(idx:idx+3, 1);
        idx = idx + 4;
        if idx > length(noise_seq) - 10
            reinit();
        end
    end

    function reinit()
        idx = 1;
    end
end