function LqrCon = con_lqr(ModelFnc, Task, ModelParam)

% linearized system around (xbar, ubar)
xbar = Task.theta_goal;
ubar = zeros(2, 1);
dfdx = ModelParam(1).A(xbar, ubar, ModelParam);
dfdu = ModelParam(1).B(xbar, ubar, ModelParam);

% design lqr
Qlqr = diag([10, 10, 1, 1]);
Rlqr = diag([1, 1]) * 20;
K = lqrd(dfdx, dfdu, Qlqr, Rlqr, Task.dt);

% return controller struct
LqrCon = struct;
LqrCon.K = K;
LqrCon.Q = Qlqr;
LqrCon.R = Rlqr;
LqrCon.get_input = @get_input;
LqrCon.name = 'lqr';

    function [u_0_, tube_center_] = get_input(x_0_, u_prev_)
        % Brief: return control input

        tube_center_ = x_0_;
        u_0_ = - K * (x_0_ - Task.theta_goal);
    end
end