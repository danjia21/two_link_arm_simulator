function [ x_fnc, dx_fnc ] = Forward_Kinematics_Fnc( theta,body)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Apply forward Kinematics in order to obtain position on velocity
% 
% Input: 
%       theta      - joint anlge and joint velocity each link
%       body       - contains dynamics and kinematics of each link
%
% Output:
%       x_fnc, dx_fnc - function handles to position and velocity of end
%                       effector in cartesians
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

NumJoint = size(theta,1)/2;

% Calculate position of every link
end_points = sym(zeros(3,NumJoint+1));
for cnt = 1:NumJoint
    end_points(:,cnt) = sym(body(cnt).kin.I_r_O);    
end
temp = body(end).kin.A_IB*sym([body(end).param.l;0;0]) + body(end).kin.I_r_O;
end_points(:,end) = sym(temp);

% Get Jacobian in order to calculate the velocities ( dr = J*dq )
a = sym(zeros(3,NumJoint));
J = [];
for cnt = 1:NumJoint
    z_cnt = body(cnt).kin.A_IB(:,3);
    a(:,cnt) = cross(z_cnt,(end_points(:,end)-end_points(:,cnt)));
    J = [J, a(:,cnt)];
end

J = J(1:2,:);
dx = J*theta(NumJoint+1:end);
dx_fnc = matlabFunction(dx);

% Calculate position in cartesians
x_ = end_points(:,end);
x_ = x_(1:2,:);
x_fnc = matlabFunction(x_); % eliminate third dimension


end

