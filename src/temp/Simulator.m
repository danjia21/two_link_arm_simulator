function sim_out = Simulator(Model_Param,Task,Controller, RandVar)

%% Filter Structure
Filter = struct;
Filter.LowPassActivated = 0; % 1 = input will be passed through a low-pass filter, 0 otherweise
Filter.LowPassTau = 0.02; % filter time constant
Filter.T = 0.01; % time [s] between samples, as in Task.dt
Filter.TimeDelay = 0; % time delay [s]

%%
% tspan = Task.start_time:Task.dt:Task.goal_time;
% x0 = Task.start_theta;
tspan = Task.t_start:Task.dt:Task.t_goal;
x0 = Task.theta_start;

RandVar.reinit();
sim_out = arm_simulation(x0,tspan,Controller,Task.ModelFnc,...
    Task,Filter,Model_Param,RandVar);

end