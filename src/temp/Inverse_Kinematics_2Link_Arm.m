function [ theta ] = Inverse_Kinematics_2Link_Arm( l1,l2,pos )
%INVERSE_KINEMATICS Summary of this function goes here
%   Detailed explanation goes here
% can be found in the book craig

nom = pos(1,:).^2 + pos(2,:).^2 - l1^2 - l2^2;
denom = 2*l1*l2;
theta2 = acos(nom/denom);
%theta2 = round(theta2.*10^4)./10^4

alpha = atan2(pos(2,:),pos(1,:));
nom = pos(1,:).^2 + pos(2,:).^2 + l1^2 - l2^2;
denom = 2*l1*sqrt(pos(1,:).^2 + pos(2,:).^2);
beta = acos(nom./denom);
%beta = round(beta.*10^4)./10^4

theta1 = alpha - beta;
theta = [theta1; theta2];
theta/pi*180;


end

