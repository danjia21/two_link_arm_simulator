function myplots(sim_out_many)

figure();  %clf

my_colors = ['r', 'g', 'b'];
names = cell(1, length(sim_out_many));

for i = 1:length(sim_out_many)
    sim_out = sim_out_many(i);
    my_color = my_colors(i);

    subplot(2,3,1);  plot(sim_out.t,sim_out.x(1,:),my_color)
    xlabel('time [sec]');  ylabel('q1 [rad]');  grid on; hold on
    subplot(2,3,2);  plot(sim_out.t,sim_out.x(2,:),my_color)
    xlabel('time [sec]');  ylabel('q2 [rad]');  grid on; hold on
    subplot(2,3,4);  plot(sim_out.t,sim_out.x(3,:),my_color)
    xlabel('time [sec]');  ylabel('dq1 [rad/s]');  grid on; hold on
    subplot(2,3,5);  plot(sim_out.t,sim_out.x(4,:),my_color)
    xlabel('time [sec]');  ylabel('dq2 [rad/s]');
    if i == 3
        plot(sim_out.tu, sim_out.x0_all(4, :), 'b--');
    end
    grid on; hold on

    % figure(2);  %clf
    subplot(2,3,3);  plot(sim_out.tu,sim_out.u(1,:),my_color,...
        sim_out.t, sim_out.u_real_all(1,:), strcat(my_color, '--'));
    xlabel('time [sec]');  ylabel('\tau_{1} [N.m]'); grid on; hold on
    subplot(2,3,6);  plot(sim_out.tu,sim_out.u(2,:),my_color, ...
        sim_out.t, sim_out.u_real_all(2,:), strcat(my_color, '--'))
    xlabel('time [sec]');  ylabel('\tau_{2} [N.m]'); grid on; hold on

    names(i) = {sim_out.Controller.name};
end

subplot(2,3,1);
legend(names);

end

