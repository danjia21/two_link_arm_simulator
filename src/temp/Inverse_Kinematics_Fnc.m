function [ th_fnc,  d_th_fnc] = Inverse_Kinematics_Fnc( ModelParam,x,body )
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Apply inverse Kinematics in order to obtain joint angles and velocities
% 
% Input: 
%       ModelParam - Parameter of n-link Model
%       x          - position and velocity of end
%                    effector in cartesians as symbolic
%       body       - contains dynamics and kinematics of each link
%
% Output:
%       th_fnc      - Joint angles function
%       d_th_fnc    - Joint angles velocity function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

NumJoint = size(x,1)/2;

% Caution function is only for 2 link robot!
th = Inverse_Kinematics_2Link_Arm(ModelParam(:).l,x(1:2,:));
th_fnc = matlabFunction(th);

% Calculate position of every link
end_points = sym(zeros(3,NumJoint+1));
% body(cnt).kin.I_r_O is the body frame origin in Predecessor frame
for cnt = 1:NumJoint
    end_points(:,cnt) = sym(body(cnt).kin.I_r_O);    
end
%
temp = body(end).kin.A_IB*sym([body(end).param.l;0;0]) + body(end).kin.I_r_O;
end_points(:,end) = sym(temp);

% Get Jacobian in order to calculate the joint velocities ( dq = inv(J)*dr )
a = sym(zeros(3,NumJoint));
J = [];
for cnt = 1:NumJoint
    z_cnt = body(cnt).kin.A_IB(:,3);
    a(:,cnt) = cross(z_cnt,(end_points(:,end)-end_points(:,cnt)));
    J = [J, a(:,cnt)];
end

J = J(1:2,:);
d_th = J\x(NumJoint+1:end);
d_th_fnc = matlabFunction(d_th);


end

