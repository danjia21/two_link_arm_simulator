close all
clear all
clc

addpath([pwd '/arm_model_functions']);
addpath([pwd '/fixed_step_odes'])


%% Model Parameters
g = 9.81;
numberOfJoints = 2;

% Define in this section the parameters to describe
% the link properties
% i.e.      l: length to next joint
%           s: offset CoG
%           m: link mass
%           Th: inertia matrix of link
%           b: joint friction
ModelParam = struct;
ModelParam.l  = 0;
ModelParam.l_m  = 0;
ModelParam.m  = 0;
ModelParam.Th = zeros(3,3);%zeros(2,1);
ModelParam.b = zeros(2,2);
ModelParam.g  = g;
ModelParam.max_motor_torque = 5;

l_s = 0.31; % length of upper arm in meters
l_e = 0.34; % length of lower arm in meters
l_ms = 0.165; % offset CoG upper arm
l_me = 0.19; % offset CoG lower arm
l_ = [l_s, l_e];
l_m = [l_ms, l_me];

%m_s   = 1.93; % segment mass upper arm in kg
%m_e   = 1.52; % segment mass lower arm in kg
m_s   = 3.051; % segment mass upper arm in kg
m_e   = 1.823; % segment mass lower arm in kg
m = [m_s, m_e];

%i_s   = 0.0141;  % segment moment of inertia
%i_e   = 0.0188;
I = zeros(3,3,2);
I(:,:,1) = diag([0.0625 0.4006 0.4019],0); % moment of inertia upper arm [kgm²]
I(:,:,2) = diag([0.0179 0.0350 0.0326],0);


for cnt = 1:numberOfJoints
    ModelParam(cnt).l  = l_(cnt);
    ModelParam(cnt).s  = l_m(cnt);
    ModelParam(cnt).m  = m(cnt);
    ModelParam(cnt).Th = I(:,:,cnt);
end

ModelParam(1).A = @(x,u,param) [Arm_f1_q(x(1:2),x(3:4),u),Arm_f1_dq(x(1:2),x(3:4),u);...
    Arm_f2_q(x(1:2),x(3:4),u),Arm_f2_dq(x(1:2),x(3:4),u)];
ModelParam(1).B = @(x,u,param)  [Arm_f1_tau(x(1:2),x(3:4),u); Arm_f2_tau(x(1:2),x(3:4),u)];
ModelParam(1).C = @(x,u,param) 0;

%% Task Structure
Task = struct;
Task.dt         = 0.1;
Task.x_start    = [-l_e;l_s;0;0];
Task.theta_start = zeros(4,1);
Task.t_start = 0;
Task.x_goal   = [0; l_s + l_e; zeros(2,1)];
Task.theta_goal = zeros(4,1);
Task.t_goal  = 5;
Task.n_state = 2*numberOfJoints;
Task.n_input = numberOfJoints;
Task.N_joints = numberOfJoints;
Task.random = [];
Task.noise_insertion_method = '';
Task.n_time = (Task.t_goal - Task.t_start)/Task.dt + 1;
Task.tspan = Task.t_start:Task.dt:Task.t_goal;

body = load('kin.mat');
body = body.kin;

% Generate function for inverse kinematics
x_ = sym(zeros(Task.n_state,1));
for cnt = 1:Task.n_state
    x_(cnt)  = sym(sprintf('x%d',cnt),'real');
end
[ th_fnc,  d_th_fnc] = Inverse_Kinematics_Fnc( ModelParam,x_,body );
Task.Inverse_Kinematics = cell(2,1);
Task.Inverse_Kinematics{1} = th_fnc;
Task.Inverse_Kinematics{2} = d_th_fnc;

% Compute initial condition in terms of joint angle and velocity
th = Task.Inverse_Kinematics{1}(Task.x_start(1),Task.x_start(2));
d_th = Task.Inverse_Kinematics{2}(th(1),th(2),Task.x_start(3),Task.x_start(4));
theta0 = real([th;d_th]);
Task.theta_start = theta0;

% Compute final condition in terms of joint angle and velocity
th = Task.Inverse_Kinematics{1}(Task.x_goal(1),Task.x_goal(2));
d_th = Task.Inverse_Kinematics{2}(th(1),th(2),Task.x_goal(3),Task.x_goal(4));
thetaEnd = real([th;d_th]);
Task.theta_goal = thetaEnd;

% Generate function for forward kinematics
theta_ = sym(zeros(Task.n_state,1));
for cnt = 1:Task.n_state
    theta_(cnt)  = sym(sprintf('th%d',cnt),'real');
end
[ x_fnc,  d_x_fnc] = Forward_Kinematics_Fnc(theta_,body);
Task.Forward_Kinematics = cell(2,1);
Task.Forward_Kinematics{1} = x_fnc;
Task.Forward_Kinematics{2} = d_x_fnc;

Task.ModelFnc.M = @Arm_M;
Task.ModelFnc.B = @Arm_B;
Task.ModelFnc.G = @Arm_G;
Task.ModelFnc.S = @Arm_S;

B_goal = Arm_B(thetaEnd(1:2), thetaEnd(3:4));

LqrCon = con_lqr(Task.ModelFnc, Task, ModelParam);
MpcCon = con_mpc(Task.ModelFnc, Task, ModelParam);
TubeMpcCon = con_mpc(Task.ModelFnc, Task, ModelParam, true);

%% Plots
RandVar = noise_sequence();
sim_out_lqr = Simulator(ModelParam,Task,LqrCon, RandVar);
sim_out_mpc = Simulator(ModelParam,Task,MpcCon, RandVar);
% sim_out_tube_mpc = Simulator(ModelParam,Task,TubeMpcCon, RandVar);
myplots([sim_out_lqr, sim_out_mpc]);%, sim_out_tube_mpc]);
% myAnimation(body,sim_out.x(1:2,:),Task.dt)
