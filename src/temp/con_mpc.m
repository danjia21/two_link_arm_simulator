function MpcCon = con_mpc(ModelFnc, Task, ModelParam, opt1)

% tube_mpc=true if use tube_mpc
if nargin < 4
    tube_mpc = false;
else
    tube_mpc = opt1;
end

% constant (global variable, do not change name)
nu = 2;
nx = 4;

% mpc setup (global variable, do not change name)
N = 20;
Q = diag([10, 10, 1, 1]);
R = diag([1, 1]) * 10;

% solve discrete-time algebraic Riccati equations for final cost
% (global variable, do not change name)
[Ad0, Bd0, ~] = get_linearized_system(zeros(nx, 1), zeros(nu, 1));
Qf = dare(Ad0, Bd0, Q, R);

% tube-mpc related design
if tube_mpc
    LqrCon = con_lqr(ModelFnc, Task, ModelParam);
    K = LqrCon.K;
    max_state_error = [0.5; 0.5; 0.3; 0.3];
    max_u_tube = K * max_state_error;
end

% setup optimizer
[A_qp_s, b_qp_s] = get_static_mpc_model();
ops =  optimset('Display','off');
warning('off', 'all');

% return controller struct
MpcCon = struct;
MpcCon.N = N;
MpcCon.Q = Q;
MpcCon.Qf = Qf;
MpcCon.R = R;
MpcCon.get_input = @get_input;
MpcCon.name = 'mpc';
if tube_mpc
    MpcCon.K = K;
    MpcCon.max_state_error = max_state_error;
    MpcCon.name = 'tube-mpc';
end


    % ---------------- nested functions --------------------%

    function [u_0_, tube_center_] = get_input(x_0_, u_prev_)
        % Brief: return control input
        % convert MPC to QP at current state
        [H_qp_, f_qp_, A_qp_d_, b_qp_d_, Aeq_qp_, beq_qp_] = get_dynamic_mpc_model(x_0_, u_prev_);
        A_qp_ = [A_qp_s; A_qp_d_];
        b_qp_ = [b_qp_s; b_qp_d_];

        % compute optimal control input
        % optarg = [u1; u2; u3; ... ; uN] (append with x0 for tube-mpc)
        optarg_ = quadprog(H_qp_, f_qp_, A_qp_, b_qp_, Aeq_qp_, beq_qp_, [], [], [], ops);
        tube_center_ = optarg_(end-nx+1:end, 1);
        if tube_mpc
            u_0_ = optarg_(1:nu, 1);
            u_0_ = u_0_ - K * (x_0_ - tube_center_);
        else
            u_0_ = optarg_(1:nu, 1);
        end
    end

    function [Ad_, Bd_, qd_] = get_linearized_system(x_, u_)
        % Brief: return linearized system around (x, u) according
        % to x(k+1) = Ad * x(k) + Bd * u(k) + qd
        dfdx_ = ModelParam(1).A(x_, u_, ModelParam);
        dfdu_ = ModelParam(1).B(x_, u_, ModelParam);
        m_ = expm([dfdx_, dfdu_; zeros(nu, nu+nx)]*Task.dt);
        Ad_ = m_(1:nx, 1:nx);
        Bd_ = m_(1:nx, nx+1:end);
        M_ = ModelFnc.M(x_(1:2));
        B_ = ModelFnc.B(x_(1:2), x_(3:4));
        G_ = ModelFnc.G(x_(1:2));
        S_ = ModelFnc.S(x_(1:2));
        qd_ = Task.dt * ([x_(3:4); M_\(S_'*u_-B_-G_)] - dfdx_ * x_ - dfdu_ * u_);
    end

    function [A_qp_, b_qp_] = get_static_mpc_model()
        % Brief: return x and u independent QP model of MPC. This function
        % should only be called once.

        % input constraints
        umax_ = ModelParam.max_motor_torque;
        b_u_ = [umax_; umax_];
        if tube_mpc
            b_u_ = b_u_ - max_u_tube;
        end
        b_u_ = repmat(b_u_, 2, 1);
        A_u_ = [eye(nu); -eye(nu)];
        G_u_ = get_blkdiag(A_u_, N);
        A_qp_ = [G_u_, zeros(2*N*nu, nx)];
        b_qp_ = repmat(b_u_, N, 1);
    end

    function [H_qp_, f_qp_, A_qp_, b_qp_, Aeq_qp_, beq_qp_] = get_dynamic_mpc_model(x_, u0_)
        % Brief: return x or u dependent QP model of MPC. This function needs
        % to be called for every x and u.

        % linearlize system
        x0_ = x_;
        u0_ = zeros(nu, 1);
        [Ad_, Bd_, qd_] = get_linearized_system(x0_, u0_);

        % fake model uncertainty
        % [Ad_, Bd_] = inject_model_uncertainty(Ad_, Bd_);

        % convert MPC objective (cost) to QP objective
        % [x1, x2, ... , xN] =
        % S_x * x0 + S_u * [u1, u2, ..., un] * S_q * [q, q, ..., q]
        S_x_ = zeros(N*nx, nx);
        S_u_ = zeros(N*nx, N*nu);
        S_q_ = zeros(N*nx, N*nx);
        for i_ = 1:N
            S_x_((i_-1)*nx+1:i_*nx, :) = Ad_^i_;
            for j_ = 1:i_
                S_u_((i_-1)*nx+1:i_*nx, (j_-1)*nu+1:j_*nu) = Ad_^(i_-j_) * Bd_;
                S_q_((i_-1)*nx+1:i_*nx, (j_-1)*nx+1:j_*nx) = Ad_^(i_-j_);
            end
        end
        temp_ = repmat(Q, 1, N);
        temp_ = mat2cell(temp_, size(Q,1), repmat(size(Q,2),1,N));
        Qbar_ = blkdiag(temp_{:});
        Qbar_((N-1)*nx+1:end, (N-1)*nx+1:end) = Qf;
        temp_ = repmat(R, 1, N);
        temp_ = mat2cell(temp_, size(R,1), repmat(size(R,2),1,N));
        Rbar_ = blkdiag(temp_{:});
        H_ = S_u_' * Qbar_ * S_u_ + Rbar_;
        F_ = S_x_' * Qbar_ * S_u_;
        Y_ = S_x_' * Qbar_ * S_x_ + Q;
        H_qp_ = 2 * [H_, F_'; F_, Y_];
        x_goal_ = Task.theta_goal;
        x_goal_rep_ = repmat(x_goal_, N, 1);
        temp_ = 2 * Qbar_ * (S_q_ * repmat(qd_, N, 1) - x_goal_rep_);
        f_qp_ = [S_u_' * temp_; S_x_' * temp_ - 2 * Q * x_goal_];

        % state constraints |x4| < 1.5
        temp_ = [get_blkdiag([0, 0, 0, 1], N); get_blkdiag([0, 0, 0, -1], N)];
        A_qp_ = temp_ * [S_u_, S_x_];
        b_qp_ = 1.5 * ones(2*N, 1) - temp_ * S_q_ * repmat(qd_, N, 1);
        if tube_mpc
            b_qp_ = b_qp_ - [0,0,0,1]*(max_state_error + Bd_*max_u_tube) * ones(2*N, 1);

            % A_qp_ = [zeros(2, N*nu+nx); A_qp_];
            % A_qp_(1:2, end) = [1; -1];
            % b_qp_ = [2.5 * ones(2, 1); b_qp_];
            % b_qp_ = b_qp_ - max_state_error(4) * ones(2*(N+1), 1);
        end

        % initial state constraints
        if tube_mpc
            A_x_ = [eye(nx); -eye(nx)];
            x_max_ = x0_ + max_state_error;
            x_min_ = x0_ - max_state_error;
            b_x_ = [x_max_; -x_min_];
            A_qp_ = [A_qp_; zeros(2*nx, N*nu), A_x_];
            b_qp_ = [b_qp_; b_x_];

            % no equality constrain
            Aeq_qp_ = [];
            beq_qp_ = [];
        else
            A_x_ = eye(nx);
            Aeq_qp_ = [zeros(nx, N*nu), A_x_];
            beq_qp_ = x0_;
        end
    end
end

function out_ = get_blkdiag(in_, n_)
    temp_ = repmat(in_, 1, n_);
    temp_ = mat2cell(temp_, size(in_, 1), repmat(size(in_, 2), 1, n_));
    out_ = blkdiag(temp_{:});
end

