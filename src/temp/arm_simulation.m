function sim_out = arm_simulation(x0,TimeSpan,Controller,ModelFnc,Task,Filter,ModelParam, RandVar)
    % Note:
    % 1. Change max_state_error to switch between normal mpc and tube-mpc
    % 2. Change generate_noise_sequence() to modify noise model
    % 3. Change inject_model_uncertainty() to modify model uncertainty




% main simulation
mem = 100000;    % length of vector pre-allocated
u_all = zeros(2, length(TimeSpan));
x_all = zeros(4, mem);
t_all = zeros(1, mem);
u_real_all = zeros(2, mem);
x0_all = zeros(4, length(TimeSpan));
idx_all = 1;
for i = 1:length(TimeSpan)-1
    if rem(TimeSpan(i), 0.1) <= 1e-5
        disp(TimeSpan(i));
    end

    % current state
    if i == 1
        x_i = x0;
        u_real_i = [0; 0];
    else
        x_i = x_all(:, idx_all-1);
        u_real_i = u_real_all(:, idx_all-1);
    end

    % compute input
    x_i_noisy = x_i;% + RandVar.get_state_noise();
    if i == 1
        temp_u = zeros(2, 1);
    else
        temp_u = u_all(:, i-1);
    end
    [u_i, x0_i] = Controller.get_input(x_i_noisy, temp_u);
    u_i_noisy = u_i;% + RandVar.get_input_noise();

    % numerically solve system ODE with constant input
    [tsol, xsol] = ode45(@(ti, xi) system_ode(ti, xi, u_i_noisy), ...
        [TimeSpan(i), TimeSpan(i+1)], [x_i; u_real_i]);

    % store solved solution
    u_all(:, i) = u_i;
    x0_all(:, i) = x0_i;
    t_all(:, idx_all:idx_all+length(tsol)-1) = tsol';
    x_all(:, idx_all:idx_all+length(tsol)-1) = xsol(:, 1:4)';
    u_real_all(:, idx_all:idx_all+length(tsol)-1) = xsol(:, 5:6)';
    idx_all = idx_all + length(tsol);
end
t_all = t_all(:, 1:idx_all - 1);
x_all = x_all(:, 1:idx_all - 1);
u_real_all = u_real_all(:, 1:idx_all - 1);

% simulation result
sim_out = struct;
sim_out.x = x_all;
sim_out.t = t_all;
sim_out.tu = TimeSpan;
sim_out.u = u_all;
sim_out.u_real_all = u_real_all;
sim_out.u(:,end) = sim_out.u(:,end-1);
sim_out.Controller = Controller;
sim_out.x0_all = x0_all;

    % ---------------- nested functions --------------------%

    function dx_ = system_ode(t_, x_, u_)
        % Brief: system dynamics ODE.

        dx_ = zeros(6, 1);

        % input noise
        u_real_ = x_(5:6);% + 5 / max(t_, 1) * sin(10* t_);

        % saturation
        max_motor_torque_ = ModelParam.max_motor_torque;
        u_real_ = (abs(u_real_)<=max_motor_torque_).*u_real_ ...
            + (u_real_>max_motor_torque_)*max_motor_torque_ ....
            + (u_real_<-max_motor_torque_)*(-max_motor_torque_);

        % system dynamics
        q_ = x_(1:2);
        dq_ = x_(3:4);
        M_ = ModelFnc.M(q_);        % Mass matrix
        B_ = ModelFnc.B(q_, dq_);   % Coriolis vector
        G_ = ModelFnc.G(q_);        % Gravity  vector
        S_ = ModelFnc.S(q_);        % Selection matrix
        dx_(1:4) = [dq_; M_\(S_'*u_real_-B_-G_)];

        % actuator dynamics
        tau_ = 0.0001;
        dx_(5:6) = (u_ - x_(5:6)) / tau_;
    end
end

