function out1 = Arm_B(in1,in2)
%ARM_B
%    OUT1 = ARM_B(IN1,IN2)

%    This function was generated by the Symbolic Math Toolbox version 6.0.
%    06-Aug-2014 02:11:23

dq1 = in2(1,:);
dq2 = in2(2,:);
q1 = in1(1,:);
q2 = in1(2,:);
t2 = q1+q2;
t3 = cos(t2);
t4 = sin(t2);
t5 = t4.*(1.9e1./1.0e2);
t6 = sin(q1);
t7 = t6.*(3.1e1./1.0e2);
t8 = t5+t7;
t9 = t3.*(1.9e1./1.0e2);
t10 = cos(q1);
t11 = t10.*(3.1e1./1.0e2);
t12 = t9+t11;
t13 = dq2.*t3.*(1.9e1./1.0e2);
t14 = dq2.*t4.*(1.9e1./1.0e2);
t15 = dq1.*t12;
t16 = t13+t15;
t17 = dq1.*t8;
t18 = t14+t17;
t19 = dq1.*t3.*(1.9e1./1.0e2);
t20 = t13+t19;
t21 = dq1.*t4.*(1.9e1./1.0e2);
t22 = t14+t21;
out1 = [dq1.*(t8.*t16-t12.*t18).*1.823+dq2.*(t8.*t20-t12.*t22).*1.823;...
    dq1.*(t4.*t16.*(1.9e1./1.0e2)-t3.*t18.*(1.9e1./1.0e2)).*1.823+dq2.*(t4.*t20.*(1.9e1./1.0e2)-t3.*t22.*(1.9e1./1.0e2)).*1.823];
